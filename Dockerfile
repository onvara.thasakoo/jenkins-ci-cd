FROM nginx:1.16.0-alpine
ADD /venus /var/www
#copy folder venus to /var/www
COPY nginx.conf /etc/nginx/nginx.conf
#copy file nginx.conf to /etc/nginx/nginx.conf
CMD ["nginx", "-g", "daemon off;"]